package com.leetcode.easy;

public class MaximumDepthOfBinaryTree {

	public static void main(String[] args) {
		TreeNode t1 = new TreeNode(3, new TreeNode(9), new TreeNode(20, new TreeNode(15), new TreeNode(7)));
		System.out.println(maxDepth(t1));
	}
	
	public static int maxDepth(TreeNode root) {
		if(root == null) return 0;
		
		int val1 = maxDepth(root.left);
		int val2 = maxDepth(root.right);
		return val1 > val2 ? val1+1 : val2+1;
    }
}

//class TreeNode {
//    int val;
//    TreeNode left;
//    TreeNode right;
//    TreeNode() {}
//    TreeNode(int val) { this.val = val; }
//    TreeNode(int val, TreeNode left, TreeNode right) {
//        this.val = val;
//        this.left = left;
//        this.right = right;
//    }
//}
