package com.leetcode.easy;

public class ImplementStrStr {

	public static void main(String[] args) {
		int result = strStr("a","a");
		System.out.println(result);
	}

	public static int strStr(String haystack, String needle) {
		if(needle.equals("")) return 0;
		if(needle.length() > haystack.length()) return -1;
		
		//2 ms
//		int i = 0;
//		int j = 0;
//		while(i < haystack.length()) {
//			if(haystack.charAt(i) == needle.charAt(j) && haystack.length()-i >= needle.length()) {
//				String temp = haystack.substring(i, i+needle.length());
//				if(temp.equals(needle))
//					return  i;
//			}
//			i++;
//		}
//		
//		return -1;
		
		//0 ms
		for(int i =0; i <= haystack.length()-needle.length(); i++) {
			if(haystack.substring(i, i+needle.length()).equals(needle)) {
				return i;
			}
		}
        return -1;
    }
}
