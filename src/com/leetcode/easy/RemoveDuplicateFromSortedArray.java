package com.leetcode.easy;

public class RemoveDuplicateFromSortedArray {

	public static void main(String[] args) {
		int[] nums = {1,1,2};
		int len = removeDuplicates(nums);
		System.out.println(len);
		System.out.println("*****************");
		for(int i =0; i<len;i++) {
			System.out.println(nums[i]);
		}
		
	}

	public static int removeDuplicates(int[] nums) {
		if(nums.length == 0) return 0;
		
		int count = 0;
		for(int i = 1; i < nums.length; i++) {
			if(nums[i] != nums[count]) {
				count++;
				nums[count] = nums[i];
			}
		}
		
		return count+1;
    }
}
