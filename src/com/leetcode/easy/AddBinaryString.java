package com.leetcode.easy;

public class AddBinaryString {

	public static void main(String[] args) {
		String result = addBinary("1010", "1011");
		System.out.println(result);
	}

	public static String addBinary(String a, String b) {
	
		StringBuilder result = new StringBuilder();
		int sum = 0;
		int i = a.length()-1;
		int j = b.length()-1;
		while(i >= 0  || j >= 0 ) {
			if(j >= 0) {
				sum += b.charAt(j) - '0';//subtract '0' to get the int value of the char from the ascii
				j--;
			}
			if(i >= 0) {
				sum += a.charAt(i) - '0';
				i--;
			}
			result.append(sum%2);
			sum /= 2;
		}
		if(sum != 0) {
			result.append("1");
		}
	return result.reverse().toString();
    }
}
