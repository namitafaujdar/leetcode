package com.leetcode.easy;

public class ReverseInteger {

	//Overflow and underflow conditions need to take care
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int result = reverse(1534236469);
		System.out.println(result);
	}
	
//	Let's take positive numbers for example. 
//	The MAX integer possible with 32-bits is 2^31 -1 or 2,147,483,647.
//	Hence MAX / 10 would be 2,147,483,640. Now if pop is anything 
//	between 0-7, then we are good since we are adding the value of 
//	pop to rev * 10, there by getting a rev value of a number equal to or 
//	less than the integer MAX. However, if the value of pop were to be 
//	greater than 7, then we are in trouble and will overflow the buffer.
//
//	In case of negative number, the MIN integer possible with 32-bits is 
//	-2^31 or -2,147,483,648. Dividing this MIN by 10 would result in a 
//	number -2,147,483,640. Again, if pop is anything between 0 and -8, 
//	we are good as we will get a number that is still "greater" than the 
//	negative minimum after doing rev * 10 + pop. However, if pop is a 
//	number small than -8, then we will overflow the buffer as will get a 
//	number than is smaller than the minimum possible integer supported by 
//	32-bits.

	public static int reverse(int x) {
		int value = 0;
		int rev = 0;
		int temp;
		while(x != 0) {
			value = x%10;
			x = x/10;
			if (rev > Integer.MAX_VALUE/10 || (rev == Integer.MAX_VALUE / 10 && value > 7)) return 0;
			if (rev < Integer.MIN_VALUE/10 || (rev == Integer.MIN_VALUE / 10 && value < -8)) return 0;
			temp = rev * 10 + value;
			rev = temp;
		}
		return rev;
        
    }

}
