package com.leetcode.easy;

import java.util.ArrayList;
import java.util.List;

public class PascalsTriangle {

	public static void main(String[] args) {
		List<List<Integer>> result = generate(5);
		for(int i = 0; i < result.size(); i++) {
			for(int j = 0; j < result.get(i).size(); j++) {
				System.out.print(result.get(i).get(j)+" ");
			}
			System.out.println();
		}

	}

	public static List<List<Integer>> generate(int numRows) {
		List<List<Integer>> ll = new ArrayList<List<Integer>>();
		List<Integer> list = new ArrayList<Integer>();
		
		for(int i = 0; i < numRows; i++) {
			list.add(0, 1);
			int k = 0;
			for(int j = 1; j <= i; j++) {
				List<Integer> value = ll.get(i-1);
				if(i-1 == 0 || k == value.size()-1) {
					list.add(j,1);
				}else {
					list.add(j,value.get(k)+value.get(k+1));
					k++;
				}
			}
			ll.add(i, list);
			list = new ArrayList<Integer>();
		}
		
		return ll;
        
    }
}
