package com.leetcode.easy;

public class MaximumSubarray {

	public static void main(String[] args) {
		int[] nums = {-2};
		int result = maxSubArray(nums);
		System.out.println(result);
	}
	
	public static int maxSubArray(int[] nums) {
		int maxCount = Integer.MIN_VALUE;
		int currCount = 0;
		
		for(int i = 0; i < nums.length; i++) {
			currCount += nums[i];

			if(currCount > maxCount) {
				maxCount = currCount;
			}
			if(currCount < 0) {
				currCount = 0;
			}
		}
		return maxCount;
    }

}
