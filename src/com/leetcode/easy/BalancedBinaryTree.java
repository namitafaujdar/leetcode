package com.leetcode.easy;

public class BalancedBinaryTree {
	static boolean result = true;
	public static void main(String[] args) {
		TreeNode t1 = new TreeNode(1, new TreeNode(2, new TreeNode(3, new TreeNode(4),new TreeNode(4)),new TreeNode(3)), new TreeNode(2));
		//TreeNode t1 = new TreeNode(3, new TreeNode(9), new TreeNode(20, new TreeNode(15), new TreeNode(7)));
		System.out.println(isBalanced(t1));
		
	}
	
	 public static boolean isBalanced(TreeNode root) {
//		if(root == null) return true;
		result = true;
		int var = checkTree(root, result);
		return result;
	    }


	private static int checkTree(TreeNode root, boolean result) {
		if(root == null) return 0;
		
		int left = checkTree(root.left, result) +1;
		int right = checkTree(root.right, result) +1;
		if(Math.abs(left - right) > 1) result = false;
		
		return left < right ? right : left;
	}


}

//class TreeNode {
//    int val;
//    TreeNode left;
//    TreeNode right;
//    TreeNode() {}
//    TreeNode(int val) { this.val = val; }
//    TreeNode(int val, TreeNode left, TreeNode right) {
//        this.val = val;
//        this.left = left;
//        this.right = right;
//    }
//}