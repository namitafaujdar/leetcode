package com.leetcode.easy;

public class RemoveDuplicatesFromSortedLinkedList {

	public static void main(String[] args) {
		ListNode l1 = new ListNode(1, new ListNode(2, new ListNode(2)));
		ListNode result = deleteDuplicates(l1);
		while(result != null) {
			System.out.println(result.val);
			result = result.next;
		}
	}
	
	public static ListNode deleteDuplicates(ListNode head) {
		if(head == null || head.next == null) return head;
		
		if(head.val < head.next.val) {
			head.next = deleteDuplicates(head.next);
			return head;
		}else {
			head = deleteDuplicates(head.next);
			return head;
		}
	}
}

	class ListNode {
	      int val;
	      ListNode next;
	      ListNode() {}
	      ListNode(int val) { this.val = val; }
	      ListNode(int val, ListNode next) { this.val = val; this.next = next; }
	  }

