package com.leetcode.easy;

import java.util.ArrayList;
import java.util.List;

public class BinaryTreeLevelOrderTraversalII {

	public static void main(String[] args) {
		TreeNode t1 = new TreeNode(3, new TreeNode(9), new TreeNode(20, new TreeNode(15), new TreeNode(7)));
		System.out.println(levelOrderBottom(t1));
	}

	public static List<List<Integer>> levelOrderBottom(TreeNode root) {
		List<List<Integer>> list = new ArrayList<>();
		
		levelOrder(root, 0 , list);
		
		return list;
    }
	
	public static void levelOrder(TreeNode root, int level, List<List<Integer>> list) {
		if(root == null) return;
		
		if(list.size() <= level) {
			list.add(0, new ArrayList<Integer>());
		}
		list.get(list.size()-1-level).add(root.val);
		
		levelOrder(root.left, level+1, list);
		levelOrder(root.right, level+1, list);
		
	}
}

//class TreeNode {
//    int val;
//    TreeNode left;
//    TreeNode right;
//    TreeNode() {}
//    TreeNode(int val) { this.val = val; }
//    TreeNode(int val, TreeNode left, TreeNode right) {
//        this.val = val;
//        this.left = left;
//        this.right = right;
//    }
//}