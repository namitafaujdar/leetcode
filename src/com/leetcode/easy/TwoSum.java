package com.leetcode.easy;
import java.util.HashMap;
import java.util.Map;

public class TwoSum {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//nums = [2, 7, 11, 15], target = 9,
		int[] nums = {3, 3};
		int[] result = twoSum(nums,6);
		for(int i =0; i < result.length; i++) {
			System.out.println(result[i]);
		}
	}
	
	//Brute force -- 85 ms
//	public static int[] twoSum(int[] nums, int target) {
//		
//		int[] result = new int[2];
//		for(int i = 0 ; i < nums.length; i++) {
//			int add = 0;
//			for(int j = 1; j < nums.length; j++) {
//				add = nums[i]+nums[j];
//				if(add == target) {
//					result[0] = i;
//					result[1] = j;
//					return result;
//				}
//			}
//		}
//		return result;
//	}
	
	//two-pass hash table  -- 2 ms
//	public static int[] twoSum(int[] nums, int target) {
//		
//		Map<Integer,Integer> map = new HashMap<Integer,Integer>();
//		for(int i = 0; i < nums.length; i++) {
//			map.put(nums[i],i);
//		}
//		for(int i = 0; i < nums.length; i++) {
//			int complement = target - nums[i];
//			if(map.containsKey(complement) && map.get(complement) != i) {
//				return new int[] {i, map.get(complement)};
//			}
//		}
//		return new int[] {};
//	}
	
	//one-pass hash table -- 1 ms
	public static int[] twoSum(int[] nums, int target) {
		Map<Integer, Integer> map = new HashMap<>();
		for(int i = 0; i < nums.length; i++) {
			int complement = target - nums[i];
			if(map.containsKey(complement) && map.get(complement) != i)
				return new int[] {map.get(complement),i};
			map.put(nums[i], i);
		}
		return new int[] {};
	}

}
