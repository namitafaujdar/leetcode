package com.leetcode.easy;
import java.util.HashMap;
import java.util.Map;

public class RomanToInt {

	public static void main(String[] args) {
		int r = romanToInt("MCMXCIV");
		System.out.println(r);

	}

	public static int romanToInt(String s) {
		if(s == null || s.length()==0) return 0;
		Map<Character, Integer> roman_numbers = addRomanNumbers();
		if(s.length() == 1) {
			return roman_numbers.get(s.charAt(0));
		}
		int result = 0;
		int curr;
		int prevChar = roman_numbers.get(s.charAt(0));
		
		for(int i = 1; i < s.length(); i++) {
			curr = roman_numbers.get(s.charAt(i));
			
			if(prevChar < curr) {
				result -= prevChar;
			}else {
				result += prevChar;
			}
			prevChar = curr;
			if(i == s.length() - 1) return result+curr;
		}
		return result;
        
    }

	private static Map<Character, Integer> addRomanNumbers() {
		Map<Character, Integer> roman_numbers = new HashMap<>();
		roman_numbers.put('I',1);
		roman_numbers.put('V',5);
		roman_numbers.put('X',10);
		roman_numbers.put('L',50);
		roman_numbers.put('C',100);
		roman_numbers.put('D',500);
		roman_numbers.put('M',1000);
		return roman_numbers;
		
	}
}
