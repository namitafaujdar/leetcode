package com.leetcode.easy;

public class MergeSortedArray {

	public static void main(String[] args) {
		int[] nums1 = {1,2,0,0,0,0};
		int[] nums2 = {2,5,6};
		merge(nums1, 2, nums2, 3);
	}

	public static void merge(int[] nums1, int m, int[] nums2, int n) {
        
		int i = m-1;
		int j = n-1;
		int together = m+n-1;
		
		while(i >= 0 && j >= 0) {
			if(nums1[i] > nums2[j]) {
				nums1[together] = nums1[i];
				
				i--;
			}else {
				nums1[together] = nums2[j];
				j--;
			}
			together--;
		}
		
		while(j >= 0) {
			nums1[together] = nums2[j];
			together--;
			j--;
		}
		
		for(int k = 0; k < nums1.length; k++) {
			System.out.println(nums1[k]);
		}
    }
}
