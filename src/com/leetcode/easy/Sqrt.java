package com.leetcode.easy;

public class Sqrt {

	public static void main(String[] args) {
		int result = mySqrt(10);
		System.out.println(result);
	}

	public static int mySqrt(int x) {
		if(x == 0) return 0;
		int low = 1;
		int high = x;
		
		while(low < high) {
			int mid = (low + high) / 2;
			if(mid <= (x/mid) && (mid+1) > (x/(mid+1))) {
				return mid;
			}else if(mid > (x/mid)) {
				high = mid - 1;
			}else {
				low = mid + 1;
			}
		}
		
		return low;
    }
}
