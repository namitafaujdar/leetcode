package com.leetcode.easy;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

//how many chars to remove to make the string of chars to be even-- 
//eg, abcbbca -- o/p 1 (b=3 is odd) remove 1 b and b's will also be even
public class Solution {

	public static void main(String[] args) {
		String s = "";
//		for(int i = 0; i <200000; i++) {
//			s += "a";
//		}
		System.out.println(solution(s));

	}

	public static int solution(String s) {
		if(s == null) {
			return 0;
		}
		Map<Character, Integer> hMap = new HashMap<Character, Integer>();
		for(int i = 0; i < s.length(); i++) {
			Character c = s.charAt(i);
			if(hMap.containsKey(c)) {
				hMap.put(c, hMap.get(c)+1);
			}else {
				hMap.put(c,  1);
			}
		}
		int count = 0;
		for (Entry<Character, Integer> mapValues : hMap.entrySet()) {
			if(mapValues.getValue() % 2 != 0) {
				count++;
			}
		}
		return count;	
	}
}
