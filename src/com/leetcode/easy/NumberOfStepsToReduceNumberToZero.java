package com.leetcode.easy;

public class NumberOfStepsToReduceNumberToZero {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int res = numberOfSteps(123);
		System.out.println(res);
	}

	public static int numberOfSteps (int num) {
		int count = 0;
		while(num != 0) {
			if(num%2 == 0) {
				num /= 2;
			}else{
				num--;
			}
			count++;
		}
		
        return count;
    }
}
