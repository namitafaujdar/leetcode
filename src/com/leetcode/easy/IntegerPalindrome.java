package com.leetcode.easy;

public class IntegerPalindrome {

	public static void main(String[] args) {
		boolean ans = isPalindrome(121);
		System.out.println(ans);

	}

	//13 ms
//	public static boolean isPalindrome(int x) {
//		int result = 0;
//		int value;
//		int check = x;
//		if(x < 0) {
//			return false;
//		}
//		while(x != 0) {
//			value = x % 10;
//			x = x / 10;
//			result = result * 10 + value;
//		}
//		if(check == result) return true;
//		
//		return false;
//        
//    }
	
	//6 ms
	public static boolean isPalindrome(int x) {
		int result = 0;
		if(x < 0 || (x % 10 == 0 && x != 0) ) {
			return false;
		}
		while(x > result) {
			int value = x % 10;
			result = result * 10 + value;
			x = x / 10;
		}
		return x == result || x == result/10;
    }
}
