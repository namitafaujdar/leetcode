package com.leetcode.easy;

public class ClimbingStairs {

	public static void main(String[] args) {
		int result = climbStairs(6);
		System.out.println(result);
	}
	
	public static int climbStairs(int n) {
		if(n == 0 || n == 1) return n;
		int prev1 = 1;
		int prev2 = 1;
		int sum = 0;
		int temp;
		
		for(int i = 2; i <= n ; i++) {
			sum = prev1+prev2;
			temp = prev2;
			prev2 = sum;
			prev1 = temp;
		}
		return sum;
    }

}
