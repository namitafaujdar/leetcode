package com.leetcode.easy;

import java.util.ArrayList;
import java.util.List;

public class PascalsTriangleII {

	public static void main(String[] args) {
		List<Integer> result = getRow(1);
		for(int i = 0; i < result.size(); i++) {
				System.out.print(result.get(i)+" ");
			}
			System.out.println();
		}

	//one way

	public static List<Integer> getRow(int rowIndex) {
		List<List<Integer>> ll = new ArrayList<List<Integer>>();
		List<Integer> list = new ArrayList<Integer>();
		
		for(int i = 0; i <= rowIndex; i++) {
			list.add(0, 1);
			int k = 0;
			for(int j = 1; j <= i; j++) {
				List<Integer> value = ll.get(i-1); //previous row
				if(i-1 == 0 || k == value.size()-1) {
					list.add(j,1);
				}else {
					list.add(j,value.get(k)+value.get(k+1));
					k++;
				}
			}
			if(i == rowIndex) return list;
			ll.add(i, list);
			list = new ArrayList<Integer>();
		}
		
		return list;
    }
	
}
