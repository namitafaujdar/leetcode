package com.leetcode.easy;

public class MinimumDepthOfBinaryTree {

	public static void main(String[] args) {
		TreeNode t1 = new TreeNode(3, new TreeNode(9), new TreeNode(20, new TreeNode(15), new TreeNode(7)));
		//TreeNode t1 = new TreeNode(3, new TreeNode(9), null);
		System.out.println(minDepth(t1));
	}

	public static int minDepth(TreeNode root) {
		if(root == null) return 0;
        
		if(root.left==null) return minDepth(root.right)+1;
		if(root.right==null) return minDepth(root.left)+1;
		
		return 1 + Math.min(minDepth(root.left), minDepth(root.right));
    }
}

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode() {}
    TreeNode(int val) { this.val = val; }
    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}
