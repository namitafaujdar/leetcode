package com.leetcode.easy;

public class SearchInsertPositionArray {

	public static void main(String[] args) {
		int[] nums = {1,2,4,5};
		int result = searchInsert(nums,3);
		System.out.println(result);
	}
	
	public static int searchInsert(int[] nums, int target) {
		if(nums.length == 0) return 0;
		
		int low = 0;
		int high = nums.length-1;
		
		while(low <= high) {
			int mid = (low+high)/2;
			if(target < nums[mid]) {
				high = mid-1;
			}else if(target > nums[mid]) {
				low = mid+1;
			}else {
				return mid;
			}
		}
		return low;
	
        
    }

}
