package com.leetcode.easy;

public class ConvertSortedArrayToBSTree {

	public static void main(String[] args) {
		int[] nums = {-10,-3,0,5,9};
		System.out.println(sortedArrayToBST(nums));
	}

	public static TreeNode sortedArrayToBST(int[] nums) {
		if(nums.length == 0) return null;
		int low = 0;
		int high = nums.length-1;
		TreeNode root = createTree(low,high, nums);
		return root;
        
    }

	private static TreeNode createTree(int low, int high, int[] nums) {
		if(low > high) return null;
		
		int mid = (low + high) / 2;
		
		TreeNode left = createTree(low, mid-1, nums);
		TreeNode right = createTree(mid+1, high, nums);
		
		return new TreeNode(nums[mid], left, right);
	}
}

//class TreeNode {
//    int val;
//    TreeNode left;
//    TreeNode right;
//    TreeNode() {}
//    TreeNode(int val) { this.val = val; }
//    TreeNode(int val, TreeNode left, TreeNode right) {
//        this.val = val;
//        this.left = left;
//        this.right = right;
//    }
//}