package com.leetcode.easy;

public class LongestCommonPrefix {

	public static void main(String[] args) {
		String res = longestCommonPrefix(new String []{"d","da"});
		System.out.println(res);
	}
	
	public static String longestCommonPrefix(String[] strs) {
		
			if(strs.length == 0) return "";
			if(strs.length == 1) return strs[0];
			
			String prev = strs[0];
			String result = "";
			int len = 0;
			
	        for(int i = 1; i < strs.length; i++){
	        	String curr = strs[i];
        		result = "";
//            	if(prev.length() <= curr.length()) {
//            	    len = prev.length();
//            	}else {
//            		len = curr.length();
//            	}
//            	for(int j = 0; j < len; j++) {
//            		if(prev.charAt(j) == curr.charAt(j)) {
//            			result += prev.charAt(j);
//            		}else {
//            			break;
//            		}
//            	}
//            	prev = result;
        		
            	//*****with while loop
            	int k = 0;
            	int j = 0;
            	while(k < prev.length() &&
            			j < curr.length() &&
            			prev.charAt(k)==curr.charAt(j)) {
            		result += prev.charAt(k);
            		k++;
            		j++;
            	}
            	prev = result;
	        }
		return result;
        
    }

}
