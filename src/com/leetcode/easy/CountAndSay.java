package com.leetcode.easy;

public class CountAndSay {

	public static void main(String[] args) {
		String s = countAndSay(6);
		System.out.println(s);

	}
	
	public static String countAndSay(int n) {
		if(n == 1) return "1";
		String result = "";
		String str = "1";
		
		for(int i = 2; i <= n; i++) {
			int frequency = 1;
			char prev = str.charAt(0);
			char curr;
			result = "";
			
			for(int j = 1; j < str.length(); j++) {
				curr = str.charAt(j);
				if(prev == curr) {
					frequency++;
				}else {
					result += frequency;
					result += prev;
					prev = curr;
					frequency = 1;
				}
			}
			result += frequency;
			result += prev;
			str = result;
			
		}
		
		return result;
    }

}
