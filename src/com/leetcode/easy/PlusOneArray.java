package com.leetcode.easy;

public class PlusOneArray {

	public static void main(String[] args) {
		int[] nums = {8,9,9,9};
		int[] result = plusOne(nums);
		
		for(int i = 0; i<result.length; i++) {
			System.out.println(result[i]);
		}
	}

	public static int[] plusOne(int[] digits) {
		
		for(int i = digits.length-1; i >= 0; i--) {
			if(digits[i] < 9) {
				digits[i] += 1;
				return digits;
			}else {
				digits[i] = 0;
			}
		}
		
		if(digits[0] == 0) {
			int[] temp = new int[digits.length+1];
			temp[0] = 1;
			return temp;
		}

		return digits;
        
    }
}
