package com.leetcode.easy;

public class RemoveElement {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] nums = {3,2,2,3};
		int len = removeElement(nums , 3);
		System.out.println(len);
		System.out.println("*****************");
		for(int i =0; i<len;i++) {
			System.out.println(nums[i]);
		}
	}
	
	public static int removeElement(int[] nums, int val) {
		if(nums.length == 0) return 0;
		
		int count = 0;
//		for(int i = 0; i < nums.length; i++) {
//			if(nums[i] != val) {
//				nums[count] = nums[i];
//				count++;
//			}
//		}
//		return count;
		
		int len = nums.length;
		while(count < len) {
			if(nums[count] == val) {
				nums[count] = nums[len-1];
				len--;
			}else {
				count++;
			}
		}
		return count;
		
    }

}
