package com.leetcode.easy;

import java.util.Stack;


public class ValidParentheses {

	public static void main(String[] args) {
		boolean result = isValid("{[()]}");
		System.out.println(result);
		
	}

	public static boolean isValid(String s) {
		if(s.length() == 0) return true;
		
		Stack<Character> stack = new Stack<>();
		int i = 0;
		while( i < s.length()) {
			Character c = s.charAt(i);
			if(stack.isEmpty()) {
				stack.push(c);
			}else {
				Character temp = stack.peek();
				if(temp == '(' && c == ')' || temp == '{' && c == '}' ||
						temp == '[' && c == ']') {
					stack.pop();
				}else {
					stack.push(c);
				}
			}
			i++;	
		}
		return stack.isEmpty() && i==s.length() ? true : false;
        
    }

}
		
		
		
		
//		for(int i = s.length()-1; i >= 0 ; i--)
//			stack.push(s.charAt(i));
//		
//		Character prev = stack.pop();
//		if(prev == ')' || prev == '}' || prev == ']' || stack.isEmpty()) return false;
		
//		while(!stack.isEmpty()) {
//			Character curr = stack.pop();
//			
//			if(prev == '(' && curr == ')' || prev == '{' && curr == '}' ||
//					prev == '[' && curr == ']') {
//				if(!tempStack.isEmpty() && !stack.isEmpty())
//					prev = tempStack.pop();
//				else {
//					prev = curr;
//				}
//			}else {
//				tempStack.push(prev);
//				prev = curr;
//			}
//			
//		}
		
		
