package com.leetcode.easy;

public class SymmetricTree {

	public static void main(String[] args) {
		//[9,-42,-42,null,76,76,null,null,13,null,13]
		TreeNode t1 = new TreeNode(1, new TreeNode(2, new TreeNode(3), new TreeNode(4)), new TreeNode(2, new TreeNode(4), new TreeNode(3)));
		System.out.println(isSymmetric(t1));
	}

	public static boolean isSymmetric(TreeNode root) {
		
		return isMirror(root, root);
		
    }
	
	public static boolean isMirror(TreeNode root1, TreeNode root2) {
		if((root1 == null && root2 == null)) return true;
		
		if((root1 != null && root2 == null) || (root1 == null && root2 != null)) return false;
		
		if(root1.val == root2.val) {
			boolean flag1 = isMirror(root1.left, root2.right);
			boolean flag2 = isMirror(root1.right, root2.left);
			return flag1 && flag2;
		}
		return false;
	}
}

//class TreeNode {
//    int val;
//    TreeNode left;
//    TreeNode right;
//    TreeNode() {}
//    TreeNode(int val) { this.val = val; }
//    TreeNode(int val, TreeNode left, TreeNode right) {
//        this.val = val;
//        this.left = left;
//        this.right = right;
//    }
//}