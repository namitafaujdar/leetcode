package com.leetcode.easy;

public class LengthOfLastWordString {

	public static void main(String[] args) {
		int n = lengthOfLastWord("length Of Last Word");
		System.out.println(n);
	}
	public static int lengthOfLastWord(String s) {
//		if(s.isEmpty() || s.equals(null)) return 0;
//		
//		String[] sArr = s.split(" ");
//		String str = "";
//		if(sArr.length > 0)
//			 str = sArr[sArr.length-1];
//		
//		return str.length();
		
		return (s.split(" ").length == 0) ? 0 : s.split(" ")[s.split(" ").length-1].length();
        
    }

}
