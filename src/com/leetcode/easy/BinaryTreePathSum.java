package com.leetcode.easy;

public class BinaryTreePathSum {

	public static void main(String[] args) {
		//TreeNode t1 = new TreeNode(1, new TreeNode(2, new TreeNode(3, new TreeNode(4),new TreeNode(4)),new TreeNode(3)), new TreeNode(2));
		TreeNode t1 = new TreeNode(3, new TreeNode(9), new TreeNode(20, new TreeNode(15), new TreeNode(7)));
		System.out.println(hasPathSum(t1, 29));
	}
	
	public static boolean hasPathSum(TreeNode root, int sum) {
		if(root == null) return false;
		if(sum == root.val && (root.left == null && root.right == null)) return true;
		if(sum != root.val && (root.left == null && root.right == null)) return false;
		
		return hasPathSum(root.left, sum - root.val) || hasPathSum(root.right, sum - root.val);
    }

}
